
#include <iostream>

#include "element.h"
#include "KGTModel.h"
#include "PlanarGrowth.h"
#include <fstream>


int main()
{
    std::cout << "Hello World!\n";

    double c0_Mn = .08; double k0_Mn = (.08 / .21137); double m0_Mn = -(1 / 0.000381);
    element Mn = element("Mn", c0_Mn, k0_Mn, m0_Mn);

    double c0_Ce = .1; double k0_Ce = .1 / .26955; double m0_Ce = -(1 / 0.000492);
    element Ce = element("Ce", c0_Ce, k0_Ce, m0_Ce);

    std::vector <element> elements{Ce , Mn};


    PlanarGrowth Al10 = PlanarGrowth(elements);


    KGTModel Al10_KGT = KGTModel(elements, 1219.1, 1.3619952017848289e-07, 5e-9, 4000, 5e-9, 4080, 28238, 9e-4, 700);

    

    std::vector<double> Velocity{1};


    std::ofstream myFile;
    myFile.open("Test.csv");
    myFile << "V, Planar, KGT\n";

    for (int i = 0; i < Velocity.size(); i++)
    {
        double Temp = Al10.Undercooling(550, 5e-9, Velocity[i], 5e-9, 10);
        double Temp_KGT = Al10_KGT.dendriteTip(1e4, Velocity[i]);


        myFile << Velocity[i];
        myFile << ",";
        myFile << Temp;
        myFile << ",";
        myFile << Temp_KGT;
        myFile << "\n";

    }

    myFile.close();

    getchar();
    
}



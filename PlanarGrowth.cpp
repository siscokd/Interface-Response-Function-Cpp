#include "PlanarGrowth.h"


PlanarGrowth::PlanarGrowth(std::vector <element> elements_Input)
{
    elements = elements_Input;
}

double PlanarGrowth::calc_kV(double k0, double a0, double Vs, double Di)
{
    double Kv = (k0 + a0 * (Vs / Di)) / (1 + a0 * (Vs / Di));
    return Kv;
}

double PlanarGrowth::calc_Mv(double m0, double kV, double k0)
{
    double Mv = m0 * ((1 - kV * (1 - log(kV / k0))) / (1 - k0));
    return Mv;
}

double PlanarGrowth::Undercooling(double Ts, double a0, double Vs, double Di, double mu)
{
    double T_planar = 0;

    double Kv, Mv;

    for (int i = 0; i < elements.size(); i++)
    {
        Kv = calc_kV(elements[i].k0, a0, Vs, Di);
        Mv = calc_Mv(elements[i].m0, Kv, elements[i].k0);


        T_planar += elements[i].c0 * ((Mv / Kv) - elements[i].m0 / elements[i].k0);
    }

    T_planar += Ts;
    T_planar -= Vs / mu;

    return T_planar;
}

#pragma once
#include "element.h"
#include <math.h>
#include <vector>

class PlanarGrowth
{
public:
	PlanarGrowth(std::vector <element> elements_Input);
	double Undercooling(double Ts, double a0, double Vs, double Di, double mu);
private:
	std::vector <element> elements;
	double calc_kV(double k0, double a0, double Vs, double Di);
	double calc_Mv(double m0, double kV, double k0);

};


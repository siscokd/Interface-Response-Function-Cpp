#pragma once
#include <vector>
#include "element.h"
#include <math.h>
class KGTModel
{
public:
	double Tf, Tliq, gam, Dl, sigst = 0.025, V0, a0, rho, Lf, alpha, cl;
	KGTModel(std::vector <element> elements_Input, double Tliq_Input, double gam_Input, double Dl_Input, double V0_Input,
										double a0_Input, double rho_Input, double Lf_Input, double alpha_Input, double cl_Input);
	double dendriteTip(double G, double V);

private:
	std::vector <element> el;
	void vparams(double Vs);
	double Iv3d(double u);

	double Pe, denom, R, tol, relax, Ct, Gc, R_temp, dif, dTv, Pe_Thermal;
	double consitutional = 0, kinetics = 0, thermal = 0, curvature = 0, cell = 0, total = 0;
	bool constitutional_Check = true;
};


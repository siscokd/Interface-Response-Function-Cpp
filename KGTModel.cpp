#include "KGTModel.h"
#include <iostream>




KGTModel::KGTModel(std::vector<element> el_Input, double Tliq_Input, double gam_Input, double Dl_Input, double V0_Input, double a0_Input, double rho_Input, double Lf_Input, double alpha_Input, double cl_Input)
{
	el = el_Input;

	for (int i = 0; i < el.size(); i++)
	{
		el[i].muk = V0_Input * (1 - el[i].k0) / el[i].m0;
	}
	Tliq = Tliq_Input;
	Tf = Tliq_Input;
	gam = gam_Input;
	Dl = Dl_Input;
	V0 = V0_Input;
	a0 = a0_Input;
	rho = rho_Input;
	Lf = Lf_Input;
	alpha = alpha_Input;
	cl = cl_Input;
}

void KGTModel::vparams(double V)
{
	double temp = a0 * V / Dl;

	for (int i = 0; i < el.size(); i++)
	{
		el[i].kv = (el[i].k0 + temp) / (1 + temp);
		el[i].mv = el[i].m0 * (1 + ((el[i].k0 - el[i].kv + el[i].kv * log(el[i].kv / el[i].k0)) / (1 - el[i].k0)));
	}

	return;
}

double KGTModel::Iv3d(double u)
{	
	double gamma = 0.5772156649015328606;
	double a = exp(-gamma);
	double b = sqrt((2 * (1 - a)) / (a * (2 - a)));
	double hinf = (1 - a) * (pow(a,2) - 6.0 * a + 12.0) / (3.0 * a * (pow((2 - a),2)) * b);
	double Test = sqrt(31.0 / 26.0);
	double q = (20.0 / 47.0) * (std::pow(u,Test));
	double h = (1 / (1 + u * sqrt(u))) + (hinf * q / (1.0 + q));
	double ytop = (exp(-u) * log(1 + (a / u) - ((1 - a) / (pow((h + b * u),2)))));
	double ybtm = a + (1.0 - a) * exp(-u / (1 - a));
	double y = u * exp(u) * ytop / ybtm;
	if (ytop < 1e-10)
		y = 0;
	return y;
}

double KGTModel::dendriteTip(double G, double V)
{

	R = 1e-8, tol = 1e-15, relax = 0.9;
	bool not_conv = true;
	
	vparams(V);

	while (not_conv) 
	{
		Pe = (R * V) / (2 * Dl);
		denom = 0;
		for (int i = 0; i < el.size(); i++)
		{
			double Testing = Iv3d(Pe);
			Ct = el[i].c0 / (1 - (1 - el[i].kv) * Iv3d(Pe));
			el[i].xic = sqrt(1 + (1 / (sigst * (Pe * Pe)))) - 1 + 2 * el[i].kv;
			el[i].xic = 1 - (2 * el[i].kv / el[i].xic);
			Gc = -(V / Dl) * Ct * (1 - el[i].kv);
			denom += (-el[i].mv * Gc * el[i].xic);
		}

		denom = sigst * (-denom - G);
		R_temp = R;
		

		try
		{
			R = sqrt(gam / denom);
			R = R_temp + relax * (R - R_temp);
			dif = abs(R - R_temp);

			//std::cout << "\n" << Pe << " " << Iv3d(Pe) << " " << R;
			//std::cout << R << "\n";
			if (dif / R < tol)
			{
				not_conv = false;
			}
			if (R != R)
			{
				not_conv = false;
			}
		}
		catch (int Attempt)
		{	
			continue;
		}


	}

	consitutional = 0;
	kinetics = 0;
	for (int i = 0; i < el.size(); i++)
	{
		double Testing = Iv3d(Pe);
		dTv = el[i].mv * el[i].c0 * (el[i].kv - 1) / el[i].kv;
		consitutional += (el[i].kv * dTv * Iv3d(Pe)) / (1 - (1 - el[i].kv) * Iv3d(Pe));
		consitutional +=  (el[i].m0 - el[i].mv) * el[i].c0;
		kinetics += V / el[i].muk;
	}

	Pe_Thermal = (V * R) / (2 * alpha);
	thermal = Lf * Iv3d(Pe_Thermal) / cl;
	

	curvature = (2 * gam / R);


	cell = G * Dl / V;


	total = consitutional + curvature + kinetics + cell + thermal;

	std::cout << R << " " << total << " " << consitutional << " " << curvature << " " << kinetics << " " << cell << " " << thermal << "\n";
	return total;
}
